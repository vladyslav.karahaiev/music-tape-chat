import {NextFunction, Request, Response} from "express";
import {OK} from "http-status-codes";
import {Message, NoId} from "../models/message";
import messageService from "../services/message-service";
import {isMessage} from "../utils/entities/isMessage";

export class MessageController {
    async getMessagesForChat(req: Request, res: Response, next: NextFunction) {
        try {
            const {chatId} = req.params;
            const parsedChatId = parseInt(chatId)
            if (isNaN(parsedChatId)) throw new Error('No userId')
            const chats = await messageService.getMessagesForChat(parsedChatId);
            res.status(OK).json(chats);
        } catch (e) {
            next(e)
        }
    }

    async addMessage(req: Request, res: Response, next: NextFunction) {
        try {
            const message: NoId<Message> = req.body;
            if (!isMessage(message)) throw new Error('Invalid message')
            const insertId = await messageService.addNewMessage(message);
            res.status(OK).json({...message, id: insertId});
        } catch (e) {
            next(e)
        }
    }
}

export default new MessageController()
