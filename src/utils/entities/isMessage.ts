import {Message, MessageStatus, NoId} from "../../models/message"


export const isMessage = (obj: any): obj is NoId<Message> => {
    return typeof obj === "object"
        && typeof obj.content === "string"
        && typeof obj.timestamp === "string"
        && typeof obj.authorId === "number"
        && typeof obj.recipientId === "number"
        && typeof obj.chatId === "number"
}
