import axios from 'axios'
import {AuthService} from "../constants/api";


class PublicKeyService {
    private publicKey?: string;

    // public async getKey(): Promise<string> {
    //     try {
    //         if (!this.publicKey) {
    //             console.log(AuthService.PUBLIC_KEY)
    //             const res = await axios.get<string>(AuthService.PUBLIC_KEY)
    //             this.publicKey = res.data;
    //         }
    //         return this.publicKey
    //     } catch (e: any) {
    //         console.log(e.message)
    //         return ""
    //     }
    // }

    public getKey(): string {
        return process.env.PUBLIC_KEY || "";
    }
}

export default new PublicKeyService()
